package fr.tokazio.chatbot;

import java.util.List;

public interface SelectedIntentGroup {

    List<Intent> intents();

    String getName();
}
