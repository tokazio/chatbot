package fr.tokazio.chatbot.demo;

import fr.tokazio.chatbot.AbstractIntent;
import fr.tokazio.chatbot.Action;
import fr.tokazio.chatbot.Context;
import fr.tokazio.chatbot.Security;

public class SecretIntent extends AbstractIntent {

    public SecretIntent() {
        super("secret");
        addTrainings("mon mot de passe pour $service est $mdp");
        addAnswers("$result");
        setAction(new Action() {
            @Override
            public String getName() {
                return "action-secret";
            }

            @Override
            public void doIt(Context context) {
                String mdp = (String) context.get("mdp");
                String service = (String) context.get("service");
                Security.singleton().setMdp("jp@" + service, mdp);
                context.set("result", "J'ai bien enregistrer votre mot de passe pour " + service);
            }
        });
    }
}
