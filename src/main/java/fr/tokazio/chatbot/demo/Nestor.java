package fr.tokazio.chatbot.demo;

import fr.tokazio.chatbot.AbstractAgent;
import fr.tokazio.chatbot.Intent;
import fr.tokazio.chatbot.demo.jenkins.JenkinsIntentGroup;
import fr.tokazio.chatbot.demo.sql.SelectIntent;

public class Nestor extends AbstractAgent {

    private final Intent defaultIntent;

    public Nestor() {
        super("Nestor");
        defaultIntent = new LearningIntent();
        addIntent(defaultIntent);
        addIntent(new HelloIntent());
        addIntent(new TimeIntent());
        addIntent(new DateIntent());
        addIntentGroup(new JenkinsIntentGroup());
        addIntent(new BodetIntent());
        addIntent(new SecretIntent());
        addIntent(new SelectIntent());
    }

    @Override
    public Intent defaultIntent() {
        return defaultIntent;
    }
}
