package fr.tokazio.chatbot.demo.sql;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import fr.tokazio.chatbot.AbstractIntent;
import fr.tokazio.chatbot.Action;
import fr.tokazio.chatbot.Context;
import org.jdbi.v3.core.Jdbi;

import java.util.List;
import java.util.Map;

public class SelectIntent extends AbstractIntent {

    public SelectIntent() {
        super("sql-select");
        addTrainings("select \"$sql\" sur $poste");
        addAnswers("$result");
        setAction(new Action() {
            @Override
            public String getName() {
                return "action-sql-select";
            }

            @Override
            public void doIt(Context context) {

                String sql = (String) context.get("sql");
                String poste = (String) context.get("poste");

                System.out.println("Connecting to '" + poste + "'...");

                try {
                    HikariConfig config = new HikariConfig();
                    config.setJdbcUrl("jdbc:oracle:thin:@" + poste + ":1521:hal1");
                    config.setUsername("erp");
                    config.setPassword("erp");
                    config.addDataSourceProperty("cachePrepStmts", "true");
                    config.addDataSourceProperty("prepStmtCacheSize", "250");
                    config.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");

                    HikariDataSource ds = new HikariDataSource(config);

                    Jdbi jdbi = Jdbi.create(ds);

                    System.out.println("Querying '" + sql + "'...");

                    List<Map<String, Object>> res = jdbi.withHandle(handle -> {
                        // Easy mapping to any type
                        return handle.createQuery("SELECT " + sql)
                                .mapToMap()
                                .list();
                    });
                } catch (Exception ex) {
                    context.set("result", "Je n'ai pas réussi à me connecter à la base de donnée du poste $poste: " + ex.getClass().getSimpleName() + " " + ex.getMessage());
                }
            }
        });
    }
}
