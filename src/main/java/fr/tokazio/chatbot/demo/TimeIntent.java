package fr.tokazio.chatbot.demo;

import fr.tokazio.chatbot.AbstractIntent;
import fr.tokazio.chatbot.Action;
import fr.tokazio.chatbot.Context;

import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeIntent extends AbstractIntent {

    private static final SimpleDateFormat DF = new SimpleDateFormat("hh:mm:ss");

    public TimeIntent() {
        super("time");
        addTrainings("quelle heure");
        addAnswers("Il est $heure");
        setAction(new Action() {

            @Override
            public String getName() {
                return "action-time";
            }

            @Override
            public void doIt(Context context) {
                context.set("heure", DF.format(new Date()));
            }
        });
    }


}
