package fr.tokazio.chatbot.demo;

import fr.tokazio.chatbot.AbstractIntent;

public class HelloIntent extends AbstractIntent {

    public HelloIntent() {
        super("hello");
        addTrainings("salut", "hello", "bonjour", "hola", "ola");
        addAnswers("Bonjour $username, que puis-je faire pour toi ?");
    }


}
