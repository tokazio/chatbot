package fr.tokazio.chatbot.demo;

import fr.tokazio.chatbot.AbstractIntent;

public class LearningIntent extends AbstractIntent {

    public LearningIntent() {
        super("learning");
        addAnswers("Je ne sais pas répondre à '$in', désolé...\n Quelle aurait dut être ma réponse ?");
    }
}
