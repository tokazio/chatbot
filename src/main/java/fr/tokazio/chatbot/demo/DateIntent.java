package fr.tokazio.chatbot.demo;

import fr.tokazio.chatbot.AbstractIntent;
import fr.tokazio.chatbot.Action;
import fr.tokazio.chatbot.Context;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateIntent extends AbstractIntent {

    private static final SimpleDateFormat DF_D = new SimpleDateFormat("EEEE dd MMMM yyyy");
    private static final SimpleDateFormat DF_H = new SimpleDateFormat("hh:mm:ss");

    public DateIntent() {
        super("date");
        addTrainings("quel jour");
        addAnswers("Nous sommes le $jour, il est $heure");
        setAction(new Action() {


            @Override
            public String getName() {
                return "action-date";
            }

            @Override
            public void doIt(Context context) {
                final Date d = new Date();
                context.set("jour", DF_D.format(d));
                context.set("heure", DF_H.format(d));
            }
        });
    }


}
