package fr.tokazio.chatbot.demo.jenkins;

import fr.tokazio.chatbot.AbstractIntent;
import fr.tokazio.chatbot.Action;
import fr.tokazio.chatbot.Context;

public class JenkinsBuildStartIntent extends AbstractIntent {

    public JenkinsBuildStartIntent() {
        super("jenkins-build-start");
        addTrainings("lance un build de $job");
        addAnswers("$result");
        setAction(new Action() {

            @Override
            public String getName() {
                return "action-jenkins-build-start";
            }

            @Override
            public void doIt(Context context) {
                boolean ok = false;
                if (ok) {
                    context.set("result", "le build de $in est lancé");
                } else {
                    context.set("result", "Désolé $username, je n'ai pas trouvé de job avec le nom '$job'");
                }
            }
        });
    }
}
