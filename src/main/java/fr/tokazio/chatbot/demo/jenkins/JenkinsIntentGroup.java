package fr.tokazio.chatbot.demo.jenkins;

import fr.tokazio.chatbot.AbstractIntentGroup;

public class JenkinsIntentGroup extends AbstractIntentGroup {

    public JenkinsIntentGroup() {
        super("jenkins");
        addKeys("build", "job");
        addIntent(new JenkinsBuildStartIntent());
        addIntent(new JenkinsBuildStateIntent());
    }


}
