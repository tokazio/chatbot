package fr.tokazio.chatbot.demo;

import fr.tokazio.chatbot.AbstractIntent;
import fr.tokazio.chatbot.Action;
import fr.tokazio.chatbot.Context;
import fr.tokazio.chatbot.Security;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

public class BodetIntent extends AbstractIntent {

    public BodetIntent() {

        super("bodet");
        addTrainings("badge pour moi");
        addAnswers("$result");
        setAction(new Action() {

            @Override
            public String getName() {
                return "action-bodet-mdp";
            }

            @Override
            public void doIt(Context context) {
                try {
                    final String k = context.get("username") + "@bodet";
                    System.out.println("Getting mdp for " + k + "...");
                    final String mdp = Security.singleton().getUserPassword(k);
                    if (mdp == null) {
                        System.out.println("\tnot found, asking for a new one...");
                        final String cle = Security.singleton().newUser(k);
                        context.set("cle", cle);
                        context.set("result", "Vous devez fournir votre mot de passe en l'encryptant en AES avec cette clé: $cle");
                    } else {
                        System.out.println("\tfound, connecting to bodet...");
                        context.set("mdp", mdp);
                        context.set("result", "Je me connecte à bodet en tant que jp avec le mot de passe $mdp et je badge");
                    }
                } catch (NoSuchAlgorithmException | BadPaddingException | IllegalBlockSizeException | InvalidKeyException | NoSuchPaddingException | InvalidKeySpecException e) {
                    e.printStackTrace();
                    context.set("result", "Erreur " + e.getClass().getSimpleName() + ": " + e.getMessage());
                }
            }
        });
    }


}
