package fr.tokazio.chatbot;

import java.util.List;

public interface IntentGroup {

    SelectedIntentGroup handleWith(Context context, String finalUserInput);

    String getName();

    List<Intent> intents();
}
