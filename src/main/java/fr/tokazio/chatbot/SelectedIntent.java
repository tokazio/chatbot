package fr.tokazio.chatbot;

import java.util.List;

public interface SelectedIntent {
    boolean hasAction();

    Action getAction();

    List<String> getAnswers();
}
