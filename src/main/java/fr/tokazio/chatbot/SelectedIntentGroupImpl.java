package fr.tokazio.chatbot;

import java.util.List;

public class SelectedIntentGroupImpl implements SelectedIntentGroup {

    private final Context context;
    private final IntentGroup intentGroup;
    private final String finalUserInput;

    public SelectedIntentGroupImpl(final Context context, final IntentGroup intentGroup, final String finalUserInput) {
        this.context = context;
        this.intentGroup = intentGroup;
        this.finalUserInput = finalUserInput;
    }

    @Override
    public List<Intent> intents() {
        return intentGroup.intents();
    }

    @Override
    public String getName() {
        return intentGroup.getName();
    }
}
