package fr.tokazio.chatbot;

import java.util.LinkedList;
import java.util.List;

public abstract class AbstractIntent implements Intent {

    private final List<String> trainings = new LinkedList<>();
    private final List<Parameter> parameters = new LinkedList<>();
    private final List<String> answers = new LinkedList<>();
    private final String name;
    private Action action;

    public AbstractIntent(final String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    protected void addTrainings(final String... strs) {
        for (String str : strs) {
            if (str != null && !str.isEmpty()) {
                trainings.add(str);
            }
        }
    }

    protected void addAnswers(String... strs) {
        for (String str : strs) {
            if (str != null && !str.isEmpty()) {
                answers.add(str);
            }
        }
    }

    protected Intent setAction(Action action) {
        if (action != null) {
            this.action = action;
        }
        return this;
    }

    @Override
    public boolean hasAction() {
        return action != null;
    }

    @Override
    public Action getAction() {
        return action;
    }

    @Override
    public List<String> getAnswers() {
        return new LinkedList<>(answers);
    }

    @Override
    public SelectedIntent handleWith(final Context context, final String finalUserInput) {
        for (String training : trainings) {
            List<Parameter> params = extractParameters(training);
            //TODO Les params sont une liste de est, on doit tous les avoirs pour traiter
            if (!params.isEmpty()) {
                boolean ok = true;
                int i = 0;
                for (Parameter p : params) {
                    System.out.println("\tEst ce que '" + p.left().trim() + "' est inclus dans '" + finalUserInput + "' ?");
                    if (finalUserInput.toUpperCase().contains(p.left().trim().toUpperCase())) {
                        System.out.println("\tOUI!");
                        int d = finalUserInput.indexOf(p.left()) + p.left().length();
                        int f;
                        if (p.isQuoted()) {
                            Parameter nextP = params.get(i + 1);
                            f = finalUserInput.indexOf(nextP.left(), d + 1);
                        } else {
                            f = finalUserInput.indexOf(' ', d + 1);
                        }
                        if (f <= 0) {
                            System.out.println("\t\tto end");
                            f = finalUserInput.length();
                        }
                        System.out.println("Extracting param " + p.name() + " value from " + d + " to " + f);
                        String v = finalUserInput.substring(d, f).trim();
                        p.value(v);
                        System.out.println("\t'" + p.name() + "' value is '" + v + "'");
                        context.set(p.name(), v);
                    } else {
                        System.out.println("\tNON!");
                        ok = false;
                        break;
                    }
                    i++;
                }
                if (ok) {

                    return new SelectedIntentImpl(context, this, finalUserInput, training);
                }
            } else {
                System.out.println("\tEst ce que '" + training + "' est inclus dans '" + finalUserInput + "' ?");
                if (finalUserInput.toUpperCase().contains(training.toUpperCase())) {
                    return new SelectedIntentImpl(context, this, finalUserInput, training);
                }
            }
        }
        return null;
    }

    private List<Parameter> extractParameters(final String str) {
        String in = str;
        System.out.println("Extracting parameters from '" + in + "'");
        final List<Parameter> params = new LinkedList<>();
        while (in.contains("$")) {
            int d = in.indexOf('$');
            System.out.println("$ @" + d);
            boolean toNextDoubleQuote = false;
            if (in.charAt(d - 1) == '"') {
                toNextDoubleQuote = true;
            }
            String s = in.substring(0, toNextDoubleQuote ? d - 1 : d);//avant le param
            System.out.println("Avant le param: " + s);
            int f;
            if (toNextDoubleQuote) {
                f = in.indexOf('"', d);
            } else {
                f = in.indexOf(' ', d);//position de fin du param
            }
            if (f < 0) {
                f = in.length();
            }
            System.out.println("Le param termine à " + f);
            String p = in.substring(d + 1, f);//nom du param
            System.out.println("Le param se nomme " + p);
            params.add(new ParameterImp(s, p, toNextDoubleQuote));
            if (f < in.length()) {
                in = in.substring(f + 1);
                System.out.println("Suivant avec '" + in + "'");
            } else {
                break;
            }
        }
        System.out.println(params);
        return params;
    }


}
