package fr.tokazio.chatbot;

import java.util.List;

public interface Intent {

    SelectedIntent handleWith(Context context, String finalUserInput);

    boolean hasAction();

    Action getAction();

    List<String> getAnswers();

    String getName();
}
