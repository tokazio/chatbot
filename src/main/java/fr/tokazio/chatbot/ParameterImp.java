package fr.tokazio.chatbot;

public class ParameterImp implements Parameter {

    private final String left;
    private final String name;
    private final boolean quoted;
    private Object value;

    public ParameterImp(String left, String paramName, boolean isQuoted) {
        this.left = left;
        this.name = paramName;
        this.quoted = isQuoted;
    }

    @Override
    public String toString() {
        return "ParameterImp{" +
                "left='" + left + '\'' +
                ", name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

    @Override
    public String left() {
        return left;
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public void value(Object value) {
        this.value = value;
    }

    @Override
    public Object value() {
        return value;
    }

    @Override
    public boolean isQuoted() {
        return quoted;
    }
}
