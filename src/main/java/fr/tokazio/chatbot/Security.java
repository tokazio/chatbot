package fr.tokazio.chatbot;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//On génére des clés de notre côté
//On donne la public au user, comme un token
//Le user crypte son mot de passe avec et nous le redonne
//On l'enregistre crypté
//Via la clé privé on saura retrouver l'original
public class Security {

    private static Security instance;

    private final Map<String, Pair<String, String>> users = new HashMap<>();

    private Security() {
        super();
        loadUsers();
    }

    public static Security singleton() {
        if (instance == null) {
            instance = new Security();
        }
        return instance;
    }

    //Pour demo seulement
    public String encryptCoteClient(String mdp, String publicKey) throws BadPaddingException, IllegalBlockSizeException, NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, InvalidKeySpecException {
        KeyFactory kf = KeyFactory.getInstance("RSA"); // or "EC" or whatever
        PublicKey pb = kf.generatePublic(new X509EncodedKeySpec(Base64.getDecoder().decode(publicKey)));
        Cipher cipher = Cipher.getInstance("RSA/ECB/OAEPWithSHA-256AndMGF1Padding");
        cipher.init(Cipher.ENCRYPT_MODE, pb);
        byte[] cipherTextBytes = cipher.doFinal(mdp.getBytes(StandardCharsets.UTF_8));
        return Base64.getEncoder().encodeToString(cipherTextBytes);
    }

    public String newUser(String username) throws NoSuchAlgorithmException {
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
        keyPairGenerator.initialize(4096);
        KeyPair keyPair = keyPairGenerator.generateKeyPair();
        String pb = Base64.getEncoder().encodeToString(keyPair.getPublic().getEncoded());
        String pv = Base64.getEncoder().encodeToString(keyPair.getPrivate().getEncoded());
        users.put(username, new Pair<>("", pv));
        saveUsers();
        return pb;
    }

    public void setMdp(String username, String encryptedMdp) {
        users.get(username).a = encryptedMdp;
        saveUsers();
    }

    private void loadUsers() {
        try {
            List<String> strs = Files.readAllLines(new File("users.txt").toPath(), StandardCharsets.UTF_8);
            for (String str : strs) {
                String[] tokens = str.split("\\|\\|");
                users.put(tokens[0], new Pair<>(tokens[1], tokens[2]));
            }
            System.out.println(users.size() + " users loaded");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void saveUsers() {
        try (FileOutputStream fos = new FileOutputStream("users.txt")) {
            for (Map.Entry<String, Pair<String, String>> e : users.entrySet()) {
                byte[] tmp = (e.getKey() + "||" + e.getValue().a + "||" + e.getValue().b + "\n").getBytes(StandardCharsets.UTF_8);
                fos.write(tmp);
            }
            System.out.println(users.size() + " users saved");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /*
    public void addUser(String username, String mdp, String privateKey) throws InvalidKeySpecException, NoSuchAlgorithmException {
        KeyFactory kf = KeyFactory.getInstance("RSA"); // or "EC" or whatever
        PrivateKey pb = kf.generatePrivate(new PKCS8EncodedKeySpec(Base64.getDecoder().decode(privateKey)));
        users.put(username, new Pair<>(mdp, pb));
    }
    */

    public String getUserPassword(String username) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException, InvalidKeySpecException {
        Pair<String, String> user = users.get(username);
        if (user != null) {
            String mdp = user.a;
            KeyFactory kf = KeyFactory.getInstance("RSA"); // or "EC" or whatever
            PrivateKey pv = kf.generatePrivate(new PKCS8EncodedKeySpec(Base64.getDecoder().decode(user.b)));
            Cipher cipher = Cipher.getInstance("RSA/ECB/OAEPWithSHA-256AndMGF1Padding");
            cipher.init(Cipher.DECRYPT_MODE, pv);
            byte[] decryptedCipherTextBytes = cipher.doFinal(Base64.getDecoder().decode(mdp));
            return new String(decryptedCipherTextBytes, StandardCharsets.UTF_8);
        }
        return null;
    }

}
