package fr.tokazio.chatbot;


import java.util.Map;
import java.util.Set;

public interface Context {


    Context set(String key, Object value);

    Set<Map.Entry<String, Object>> entries();

    Object get(String key);
}
