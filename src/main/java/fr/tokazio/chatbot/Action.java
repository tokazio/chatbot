package fr.tokazio.chatbot;

public interface Action {
    String getName();

    void doIt(Context context);
}
