package fr.tokazio.chatbot;

import java.util.List;

public class SelectedIntentImpl implements SelectedIntent {

    private final Context context;
    private final Intent intent;
    private final String finalUserInput;
    private final String token;

    public SelectedIntentImpl(final Context context, final Intent intent, final String finalUserInput, final String token) {
        this.context = context;
        this.intent = intent;
        this.finalUserInput = finalUserInput;
        this.token = token;
    }

    @Override
    public boolean hasAction() {
        return intent.hasAction();
    }

    @Override
    public Action getAction() {
        return intent.getAction();
    }

    @Override
    public List<String> getAnswers() {
        return intent.getAnswers();
    }

    @Override
    public String toString() {
        return "SelectedIntentImpl '" + intent.getName() + "'";
    }
}
