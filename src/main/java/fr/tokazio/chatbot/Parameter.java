package fr.tokazio.chatbot;

public interface Parameter {
    String left();

    String name();

    void value(Object value);

    Object value();

    boolean isQuoted();
}
