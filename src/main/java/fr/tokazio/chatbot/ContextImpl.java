package fr.tokazio.chatbot;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class ContextImpl implements Context {

    private final Map<String, Object> map = new HashMap<>();

    @Override
    public Context set(final String key, final Object value) {
        if (key != null && !key.isEmpty()) {
            if (value == null) {
                map.remove(key);
            } else {
                map.put(key, value);
            }
        }
        return this;
    }

    @Override
    public Set<Map.Entry<String, Object>> entries() {
        return map.entrySet();
    }

    @Override
    public Object get(String key) {
        if (key != null && !key.isEmpty()) {
            return map.get(key);
        }
        return null;
    }

    @Override
    public String toString() {
        return "ContextImpl{" +
                "map=" + map +
                '}';
    }
}
