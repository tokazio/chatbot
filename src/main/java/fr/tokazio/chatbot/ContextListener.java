package fr.tokazio.chatbot;

public interface ContextListener {

    void with(Context context, String answer);
}
