package fr.tokazio.chatbot;

import java.util.LinkedList;
import java.util.List;

public abstract class AbstractIntentGroup implements IntentGroup {

    private final String name;
    private final List<Intent> intents = new LinkedList<>();
    private final List<String> keys = new LinkedList<>();

    public AbstractIntentGroup(final String name) {
        this.name = name;
    }

    protected void addKeys(String... keys) {
        for (String s : keys) {
            if (s != null && !s.isEmpty()) {
                this.keys.add(s);
            }
        }
    }


    protected void addIntent(Intent intent) {
        if (intent != null) {
            intents.add(intent);
        }
    }

    @Override
    public SelectedIntentGroup handleWith(Context context, String finalUserInput) {
        for (String k : keys) {
            if (finalUserInput.contains(k)) {
                return new SelectedIntentGroupImpl(context, this, finalUserInput);
            }
        }
        return null;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public List<Intent> intents() {
        return new LinkedList<>(intents);
    }
}
