package fr.tokazio.chatbot;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public abstract class AbstractAgent implements Agent {

    private final String name;
    private final List<Intent> intentList = new LinkedList<>();
    private final List<IntentGroup> intentGroupList = new LinkedList<>();

    public AbstractAgent(final String name) {
        this.name = name;
    }

    public Agent addIntentGroup(final IntentGroup intentGroup) {
        if (intentGroup != null) {
            intentGroupList.add(intentGroup);
        }
        return this;
    }

    public Agent addIntent(final Intent intent) {
        if (intent != null) {
            intentList.add(intent);
        }
        return this;
    }

    @Override
    public Agent handle(final Context context, final String finalUserInput, final ContextListener listener) {

        context.set("in", finalUserInput);

        SelectedIntentGroup selectedGroup = null;
        SelectedIntent selected = null;

        for (IntentGroup intentGroup : intentGroupList) {
            System.out.println("IntentGroup " + intentGroup.getName() + "...");
            selectedGroup = intentGroup.handleWith(context, finalUserInput);
            if (selectedGroup != null) {
                break;
            }
        }
        System.out.println();

        if (selectedGroup != null) {
            System.out.println("Recherche d'un intent  dans le groupe '" + selectedGroup.getName() + "' pour '" + finalUserInput + "'...");
            for (Intent intent : selectedGroup.intents()) {
                System.out.println("Intent " + intent.getName() + "...");
                selected = intent.handleWith(context, finalUserInput);
                if (selected != null) {
                    break;
                }
            }
        }
        System.out.println();

        if (selected == null) {
            System.out.println("Recherche d'un intent pour '" + finalUserInput + "'...");
            for (Intent intent : intentList) {
                System.out.println("Intent " + intent.getName() + "...");
                selected = intent.handleWith(context, finalUserInput);
                if (selected != null) {
                    break;
                }
            }
        }
        System.out.println();

        if (selected == null) {
            selected = new SelectedIntentImpl(context, defaultIntent(), finalUserInput, "");
            System.out.println("Utilisation de l'intent par défaut: " + selected);
        }
        System.out.println();

        if (selected.hasAction()) {
            proceed(context, selected.getAction());
        } else {
            System.out.println("Pas d'action pour " + selected);
        }
        answer(context, finalUserInput, selected.getAnswers(), listener);
        return this;
    }


    @Override
    public Agent proceed(final Context context, final Action action) {
        System.out.println("Action en cours: " + action.getName());
        try {
            action.doIt(context);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return this;
    }

    @Override
    public Agent answer(final Context context, final String finalUSerInput, final List<String> answerList, final ContextListener listener) {
        if (!answerList.isEmpty()) {
            if (answerList.size() > 1) {
                System.out.println("Plusieurs réponses possibles");

                //TODO random

            } else {
                //On prends la 1ère et unique réponse
                String a = answerList.get(0);
                System.out.println("La réponse sera " + a);

                //Detection des params à utiliser dans la réponse
                final List<String> params = new LinkedList<>();
                int dollar;
                int pos = 0;
                do {
                    dollar = a.indexOf('$', pos);//debut du param
                    if (dollar >= 0) {
                        int end = a.indexOf(' ', dollar);//fin du param
                        if (end < 0) {
                            end = a.length();
                        }
                        String ex = a.substring(dollar + 1, end);
                        if (!ex.isEmpty()) {
                            params.add(ex.replaceAll(",$", "")
                                    .replaceAll("'$", ""));
                        }
                        pos = end;
                    }
                } while (dollar >= 0 && pos < a.length());
                System.out.println("Answer using params: " + params);
                System.out.println("Answer using context: " + context);
                //

                for (String p : params) {
                    if (context.get(p) == null) {
                        System.out.println("Le param '" + p + "' n'est pas présent dans le context");
                        a = a.replaceAll("\\$" + p, "");
                    }
                }

                while (a.contains("$")) {
                    System.out.println("La réponse sera: " + a);
                    System.out.println("Avec le contexte: " + context.entries());
                    //On remplace le $in par la demande originale
                    a = a.replaceAll("\\$in", finalUSerInput);
                    //On remplace les paramètres avec les valeurs du contexte
                    for (Map.Entry<String, Object> e : context.entries()) {
                        System.out.println("\t remplace '$" + e.getKey() + "' par '" + e.getValue().toString() + "'");
                        a = a.replace("$" + e.getKey(), e.getValue().toString());
                    }
                }
                //On réponds
                System.out.println(name + ">>" + a);
                if (listener != null) {
                    listener.with(context, a);
                }
            }
        } else {
            System.out.println("Pas de réponse possible!");
        }
        return this;
    }


}
