package fr.tokazio.chatbot;

import java.util.List;

public interface Agent {

    Intent defaultIntent();

    Agent handle(Context context, String finalUserInput, ContextListener listener);

    Agent proceed(Context context, Action action);

    Agent answer(Context context, String finalUserInput, List<String> answerList, ContextListener listener);
}
